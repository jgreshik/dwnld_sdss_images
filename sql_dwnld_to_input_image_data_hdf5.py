#!/usr/bin/env python
# coding: utf-8

# In[1]:


#### formatting data to run through autoencoder
import pandas as pa
from astropy.coordinates import SkyCoord
from astroquery.sdss import SDSS
from astropy.wcs import WCS
from astropy import units as u
from astropy.nddata import Cutout2D
import matplotlib.pyplot as plt
import numpy as np
import time
import os
import sys
import h5py
from tempfile import gettempdir 
import shutil
import urllib3

#time to run tmux detach
#time.sleep(30)

#save_path='./dump/saved_formatted/sdss_object_data_norm_clean.hdf5'
save_path='./dump/saved_formatted/reform.hdf5'

#object type to autoencode
#object_type='GALAXY'
object_types=('STAR','QSO')

#pixel value depends on radial cut
pixel_value=28
ra_cut=11.0

#only download images that have img val avgs under cutoff
cutoff=0.15

# In[2]:

def plot(objCoord,cutout):
    # Plot the image
    wcs_cut = cutout.wcs
    fig = plt.figure()
    ax = fig.add_subplot(111, projection=wcs_cut)
    imgRegion = ax.imshow(cutout.data, origin='lower', vmin=-0.64, vmax=0.45)
    ax.scatter(objCoord.ra, objCoord.dec , s=150, linewidths=0.35,  edgecolors='black', transform=ax.get_transform('world'), label='SHOC579')


# In[8]:

#normalizing data to 0-1 scale
#normalizes one image and returns normalized image
def normalize_data(data):
    #normalize all data 
    data=(data-np.min(data))/(np.max(data)-np.min(data))
    return data

#fn for determining if main object is in center of image
#partition image into 3x3 pieces
#return false if center piece does not have highest avg value
def check_center(data):
    avgs=np.empty([3,3])
    w,h=data.shape
    for i in range(3):
        for j in range(3):
            temp=data[int(i*w/3):int(i*w/3+w/3),int(j*h/3):int(j*h/3+h/3)]
            avgs[i,j]=np.mean(temp)
            #fig=plt.figure(figsize=(1,1))
            #plt.imshow(temp)
    if avgs[1,1]!=np.max(avgs): return False
    return True

def get_obj_data(ra,dec):
    # Coordinates from SDSS SQL search
    objCoord=SkyCoord(ra=ra*u.degree,dec=dec*u.degree)
    # Get SDSS images
    xObj = SDSS.query_region(objCoord, spectro=True)
    imgObj = SDSS.get_images(matches=xObj)
    image_i = imgObj[0][0]
    data_i = image_i.data
    # Get image coordinates
    wcs = WCS(image_i.header)
    # Cut the Field of view 25.0 x 25.0 arcsec
    FoV = np.array([ra_cut, ra_cut])
    FoV_dimen = u.Quantity((FoV[0], FoV[1]), u.arcsec)
    cutout = Cutout2D(data_i, objCoord, FoV_dimen, wcs=wcs)
    wcs_cut = cutout.wcs
    ret=cutout.data.astype(float)
    #normalizing cutout data
    ret=normalize_data(ret)
    #for i in cutout.data.size: ret.append(cutout.data[i])
    #plot(objCoord,cutout)
    #print(cutout.data.size)
    return ret


#fn for deleting downloaded compressed fits files after we are done with them
#need to delete ALL files in .astropy/cache/download/py3 (including .bak .dat and .dir files)
dir_name1 = '/u/mx/fa/jgreshik/.astropy/cache/download/py3/' 
dir_name2 = '/u/mx/fa/jgreshik/.astropy/cache/astroquery/SDSS/'
dir_name3 = str(gettempdir())#+'/'
dirs=(dir_name1,dir_name2,dir_name3)
def delete_fits():
    for dir_name in dirs:
        test = os.listdir(dir_name)
        for item in test:
            try:
                os.remove(os.path.join(dir_name, item))
            except IsADirectoryError:
                shutil.rmtree(os.path.join(dir_name, item))
            except PermissionError:
                continue



# ##### downloaded from DR12

# In[3]:

for object_type in object_types:
    print('#########'+object_type+'#########')
    print('#########################')
    print('#########################')
    print('#########################')

    df=pa.read_csv('./dump/'+object_type+'_data.csv', header=None)
    dataset = df.values
    ra = dataset[1:,1].astype(float)
    dec = dataset[1:,2].astype(float)

    # ##### now getting cutout data

    #getting number of images in save file
    count=0
    max_val=0
    try:
        with h5py.File(save_path, 'r') as f:
            group=object_type
            for data in f.get(group).__iter__():
                count+=1
                if(max_val<int(data)):max_val=int(data)
            print(group+" Count: "+str(count))
            print(group+" Max_val: "+str(max_val))
    except OSError:
        print('No file found. Creating save file '+save_path)
    except AttributeError:
        print('No group found. Creating group '+object_type)
    # In[33]:


    #saving collected image cutouts from sql query
    #to hdf5 file
    def save_data_to_file(count):
        #creating new dataset for each stellar object observed
        with h5py.File(save_path, 'a') as f:
            #will over-write data of similar object and i value
            group=f.require_group(object_type)
            for i in range(int(sys.argv[1]),int(sys.argv[2])):#assuming square image data
                if(max_val>i):continue
                print("Iteration "+str(i))
                try: 
                    x=get_obj_data(ra[i],dec[i])
                except ValueError:
                    print('ValueError detected. Continuing with data collection')
                    continue
                except urllib3.exceptions.ProtocolError:
                    print('ConnectionError detected. Restarting collection in 15s...')
                    time.sleep(15)
                    continue
                except urllib3.exceptions.TimeoutError:
                    print('TimeoutError detected. Restarting collection in 180s...')
                    time.sleep(180)
                    continue
                if(x.shape[0]!=pixel_value or x.shape[1]!=pixel_value):
                    print('Invalid shape. Continuing with data collection')
                    continue
                if np.mean(x)<cutoff and check_center(x):
                    #hdf5 write
                    #saving x data as a DataFrame object
                    save_dataframe=pa.DataFrame(x)
                    #save 'i' DataFrame object within object_type/i 
                    count+=1
                    print("Downloading "+object_type+" image "+str(count))
                    dset=group.require_dataset(str(i),shape=(pixel_value,pixel_value),dtype=float,compression='lzf')
                    dset[:]=save_dataframe
                #after 30 downloads remove downloaded fits files and wait 0.5s to give sdss website a break
                if i%30==0:
                    delete_fits()
                    time.sleep(5)


    # In[34]:


    save_data_to_file(count);

#for reading file
#with h5py.File(save_path, 'r') as f:
#    for group in f:
#        #print(f.get(group))
#        for data in f.get(group).__iter__():
#            print(str(group)+"\\"+str(data))
#            check=f.get(group).require_dataset(data,shape=(63,63),dtype=float)
#            print(check[0])
# In[36]:


delete_fits()
